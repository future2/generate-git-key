#!/bin/bash

function yesno(){
	local question=${1:-Continue}
	local default=${2:-Y}
	if [ -z "$2" ]; then
		local defaulttext="Y/n"
	else
		local defaulttext="y/$default"
	fi
	local yn=false;
	while ! $yn; do
		read -p "$question [$defaulttext]? " yn
		yn=${yn:-$default}
		case "$yn" in
			y|Y|Yes|yes )
				return 0;;
				#yn=true;;
			n|N|No|no )
				return 1;;
				#yn=true;;
			* )
				echo "Please enter Y[es] or N[o]."
				yn=false;;
		esac
	done
}

function userpass(){
	echo "Please enter your Bitbucket credentials below"
	read -p "Username: " bitbucketusername
	read -s -p "Password: " bitbucketpassword
}

repositoryurl=$1

if [ $(echo "$repositoryurl" | grep '.git') ]; then
	repotag="$(echo "$repositoryurl" | grep -o -P '(?<=future2/).*?(?=.git)')"
else
	repotag="$(echo "$repositoryurl" | grep -o -P '(?<=future2/).*?(?=/)')"
fi

publickey=$(cat ~/.ssh/bitbucket.pub)
sshkeyadded=false
while ! $sshkeyadded; do
	if [ -z "$bitbucketusername" ]; then
		userpass
	else
		if yesno "Would you like to re-enter your username and password for bitbucket" N; then
			userpass
		fi
	fi
	response=$(curl -X GET -s --fail -u "$bitbucketusername:$bitbucketpassword" --url "https://api.bitbucket.org/2.0/repositories/future2/$repotag/deploy-keys")
	error=$?
	if [ $error -eq 0 ]; then
		echo $response | grep -q "$publickey"
		if [ $? -eq 0 ]; then
			sshkeyadded=true
			echo "The SSH key already exists!"
		else
			echo
			curl -X POST -s -u "$bitbucketusername:$bitbucketpassword" --url "https://api.bitbucket.org/2.0/repositories/future2/$repotag/deploy-keys" --data-urlencode "key=$publickey" > /dev/null
			echo
			if [ $? -ne 0 ]; then
				echo "Could not add the SSH Key automatically!"
				if yesno "Would you like to install the key manually" N; then
					echo "Goto the below URL and add the access key:"
					echo "https://bitbucket.org/future2/vm-instance-scripts/admin/access-keys/"
					echo "---------------------------------"
					echo $publickey
					echo "---------------------------------"
					while ! yesno "Is the key added"; do
						echo "Add the key before continuing!"
					done
					curl -X GET -s -u "$bitbucketusername:$bitbucketpassword" --url "https://api.bitbucket.org/2.0/repositories/future2/$repotag/deploy-keys" | grep -q "$publickey"
					if [ $? -eq 0 ]; then
						sshkeyadded=true
						echo "The SSH key was successfully added to the repository, continuing!"
					else
						echo "The SSH key could not be found, please retry!"
					fi
				else
					echo "The SSH key must be added to the repo so you can pull the installer, please retry or install manually!"
				fi
			else
				echo "SSH key successfully added to the repository!"
				sshkeyadded=true
			fi
		fi
	else
		if [ $error -eq 22 ]; then
			echo "You have entered an incorrect username and password, please enter a new one!"
			unset bitbucketusername
		else
			echo "An unexpected error occurred... please retry"
		fi
	fi
	if ! $sshkeyadded; then
		if ! yesno "Would you like to retry"; then
			exit 1
		fi
	fi
done

exit 0
