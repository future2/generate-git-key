#!/bin/bash

function pause(){
   read -p "$*"
}

function yesno(){
	local question=${1:-Continue}
	local default=${2:-Y}
	if [ -z "$2" ]; then
		local defaulttext="Y/n"
	else
		local defaulttext="y/$default"
	fi
	local yn=false;
	while ! $yn; do
		read -p "$question [$defaulttext]? " yn
		yn=${yn:-$default}
		case "$yn" in 
			y|Y|Yes|yes )
				return 0;;
				#yn=true;;	
			n|N|No|no )
				return 1;;
				#yn=true;;
			* )
				echo "Please enter Y[es] or N[o]."
				yn=false;;
		esac
	done
}

function userpass(){
	echo "Please enter your Bitbucket credentials below so we can pull the F2 scripts"
	read -p "Username: " bitbucketusername
	read -s -p "Password: " bitbucketpassword
}

echo "Generating the SSH Key..."

if [ ! -f ~/.ssh/bitbucket ]; then
	host="$(hostname)"

	ssh-keygen -t rsa -f bitbucket -C "${host}" -q -N ''
	sudo mv bitbucket ~/.ssh/bitbucket
	sudo mv bitbucket.pub ~/.ssh/bitbucket.pub
	sudo chmod 400 ~/.ssh/bitbucket
	eval $(ssh-agent -s)
	ssh-add ~/.ssh/bitbucket
	LINE='Host bitbucket.org
	HostName bitbucket.org
	IdentityFile ~/.ssh/bitbucket
	PreferredAuthentications publickey'
	FILE=~/.ssh/config
	touch $FILE
	grep -qF "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
	echo "SSH key generated and added to the agent successfully!"
else
	echo "SSH key already exists for bitbucket, using: ~/.ssh/bitbucket"
fi
ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
publickey=$(cat ~/.ssh/bitbucket.pub)

sshkeyadded=false
while ! $sshkeyadded; do
	./addkeytorepo.sh "git@bitbucket.org:future2/vm-instance-scripts.git"
	if [ $? -eq 0 ]; then
		sshkeyadded=true
	else
		echo "You must add the key to the repo, please retry!"
	fi
done

echo "Setting up the F2 directory..."

if [ ! -d /f2 ]; then
	sudo mkdir /f2
	sudo chown f2:f2 /f2 
	sudo chmod 775 /f2
fi
git -C /f2 rev-parse
if [ $? -eq 0 ]; then
	git -C /f2 pull
else
	git clone --recursive git@bitbucket.org:future2/vm-instance-scripts.git /f2
fi

echo "F2 directory successfully setup!"

if yesno "Would you like to run the installer now"; then
	cd /f2/install
	echo
	echo
	echo
	echo
	./installer.sh
fi